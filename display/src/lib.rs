//! This lib serves as kind of an abstraction layer for the 7-Segment-Display.
use std::mem;

#[derive(Clone, Copy)]
pub enum Segment {
    A(bool),
    B(bool),
    C(bool),
    D(bool),
    E(bool),
    F(bool),
    G(bool)
}

pub struct SegmentDisplay {
    segments: [Segment; 7]
}

impl SegmentDisplay {

    /// Creates a new object,
    /// in which every segment is disabled.
    pub fn new() -> SegmentDisplay {
        SegmentDisplay {
            segments: [
                Segment::A(false),
                Segment::B(false),
                Segment::C(false),
                Segment::D(false),
                Segment::E(false),
                Segment::F(false),
                Segment::G(false)
            ]
        }
    }

    /// assign a new value to a segment
    /// ```rust
    /// let d = SegmentDisplay::new();
    /// d.assign(Segment::B(true));
    /// ```
    pub fn assign(&mut self, new_value: Segment) {
        for seg in &mut self.segments {
            // In order to compare the Enum fields without their variants,
            // the given Enum must be discriminated.
            if mem::discriminant(seg) == mem::discriminant(&new_value) {
                *seg = new_value;
            }
        }
    }

    /// Takes a new display value as argument.
    /// This trait implementing type returns a new object of SegmentDisplay which replaces the current object.
    pub fn input(&mut self, new_value: impl ValueReader) {
        let display_value = new_value.get_display();
        *self = display_value;
    }

    /// Creates a new _blank_ object of SegmentDisplay.
    /// All segments are disabled.
    pub fn clear(&mut self) {
        let blank_display = SegmentDisplay::new();
        *self = blank_display;
    }
}

pub trait ValueReader {
    /// This trait is implemented by types who represent a display value, like _Two_.
    /// Using the member method __assign__ the implementing type can enable corresponding segments.
    fn get_display(&self) -> SegmentDisplay;
}

pub trait Output {
    fn show(&self);
}

impl Output for SegmentDisplay {
    fn show(&self) {
        if let Segment::A(active) = self.segments[0] {
            if active == true { println!(" _"); } else { println!(""); }
        }
        if let Segment::F(active) = self.segments[5] {
            if active == true { print!("|"); } else { print!(" "); }
        }
        if let Segment::G(active) = self.segments[6] {
            if active == true { print!("_"); } else { print!(" "); }
        }
        if let Segment::B(active) = self.segments[1] {
            if active == true { println!("|"); } else { println!(""); }
        }
        if let Segment::E(active) = self.segments[4] {
            if active == true { print!("|"); } else { print!(" "); }
        }
        if let Segment::D(active) = self.segments[3] {
            if active == true { print!("_"); } else { print!(" "); }
        }
        if let Segment::C(active) = self.segments[2] {
            if active == true { println!("|"); } else { println!(""); }
        }
    }
}
