use crate::display::ValueReader;
use crate::display::Segment;
use crate::display::SegmentDisplay;

pub struct SegmentZero;
pub struct SegmentOne;
pub struct SegmentTwo;
pub struct SegmentThree;
pub struct SegmentFour;
pub struct SegmentFive;
pub struct SegmentSix;
pub struct SegmentSeven;
pub struct SegmentEight;
pub struct SegmentNine;

impl ValueReader for SegmentZero {
    fn get_display(&self) -> SegmentDisplay {
        let mut seg = SegmentDisplay::new();
        seg.assign(Segment::A(true));
        seg.assign(Segment::B(true));
        seg.assign(Segment::C(true));
        seg.assign(Segment::D(true));
        seg.assign(Segment::E(true));
        seg.assign(Segment::F(true));
        seg
    }
}

impl ValueReader for SegmentOne {
    fn get_display(&self) -> SegmentDisplay {
        let mut seg = SegmentDisplay::new();
        seg.assign(Segment::B(true));
        seg.assign(Segment::C(true));
        seg
    }
}

impl ValueReader for SegmentTwo {
    fn get_display(&self) -> SegmentDisplay {
        let mut seg = SegmentDisplay::new();
        seg.assign(Segment::A(true));
        seg.assign(Segment::B(true));
        seg.assign(Segment::D(true));
        seg.assign(Segment::E(true));
        seg.assign(Segment::G(true));
        seg
    }
}

impl ValueReader for SegmentThree {
    fn get_display(&self) -> SegmentDisplay {
        let mut seg = SegmentDisplay::new();
        seg.assign(Segment::A(true));
        seg.assign(Segment::B(true));
        seg.assign(Segment::C(true));
        seg.assign(Segment::D(true));
        seg.assign(Segment::G(true));
        seg
    }
}

impl ValueReader for SegmentFour {
    fn get_display(&self) -> SegmentDisplay {
        let mut seg = SegmentDisplay::new();
        seg.assign(Segment::B(true));
        seg.assign(Segment::C(true));
        seg.assign(Segment::F(true));
        seg.assign(Segment::G(true));
        seg
    }
}

impl ValueReader for SegmentFive {
    fn get_display(&self) -> SegmentDisplay {
        let mut seg = SegmentDisplay::new();
        seg.assign(Segment::A(true));
        seg.assign(Segment::C(true));
        seg.assign(Segment::D(true));
        seg.assign(Segment::F(true));
        seg.assign(Segment::G(true));
        seg
    }
}

impl ValueReader for SegmentSix {
    fn get_display(&self) -> SegmentDisplay {
        let mut seg = SegmentDisplay::new();
        seg.assign(Segment::A(true));
        seg.assign(Segment::C(true));
        seg.assign(Segment::D(true));
        seg.assign(Segment::E(true));
        seg.assign(Segment::F(true));
        seg.assign(Segment::G(true));
        seg
    }
}

impl ValueReader for SegmentSeven {
    fn get_display(&self) -> SegmentDisplay {
        let mut seg = SegmentDisplay::new();
        seg.assign(Segment::A(true));
        seg.assign(Segment::B(true));
        seg.assign(Segment::C(true));
        seg
    }
}

impl ValueReader for SegmentEight {
    fn get_display(&self) -> SegmentDisplay {
        let mut seg = SegmentDisplay::new();
        seg.assign(Segment::A(true));
        seg.assign(Segment::B(true));
        seg.assign(Segment::C(true));
        seg.assign(Segment::D(true));
        seg.assign(Segment::E(true));
        seg.assign(Segment::F(true));
        seg.assign(Segment::G(true));
        seg
    }
}

impl ValueReader for SegmentNine {
    fn get_display(&self) -> SegmentDisplay {
        let mut seg = SegmentDisplay::new();
        seg.assign(Segment::A(true));
        seg.assign(Segment::B(true));
        seg.assign(Segment::C(true));
        seg.assign(Segment::D(true));
        seg.assign(Segment::F(true));
        seg.assign(Segment::G(true));
        seg
    }
}

