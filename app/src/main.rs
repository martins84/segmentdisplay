pub use display;
mod values;

use std::{env, thread, time};
use crate::display::Output;

pub fn select(value: usize, seg_display: &mut display::SegmentDisplay) {
    match value {
        0 => seg_display.input(values::SegmentZero),
        1 => seg_display.input(values::SegmentOne),
        2 => seg_display.input(values::SegmentTwo),
        3 => seg_display.input(values::SegmentThree),
        4 => seg_display.input(values::SegmentFour),
        5 => seg_display.input(values::SegmentFive),
        6 => seg_display.input(values::SegmentSix),
        7 => seg_display.input(values::SegmentSeven),
        8 => seg_display.input(values::SegmentEight),
        9 => seg_display.input(values::SegmentNine),
        _ => panic!("Given number cannot be represented by segment display")
    }
}

fn countdown_from(value: usize) {
    let start_num = value;
    let sleep_duration = time::Duration::from_secs(1);

    let mut segm_display = display::SegmentDisplay::new();

    for i in (0..=start_num).rev().step_by(1) {
        select(i, &mut segm_display);
        segm_display.show();
        thread::sleep(sleep_duration);
    }
}

fn main() {
    let cmd_args: Vec<String> = env::args().collect();
    let arg: usize = if cmd_args.len() > 1 {
        cmd_args[1].parse().expect("Could not convert argument")
    } else {
        10
    };

    let is_countdown = if cmd_args.len() > 2 {
        cmd_args[2] == "--as-countdown"
    } else {
        false
    };
    
    if is_countdown == true {
        countdown_from(arg);
    } else {
        let mut segm_display = display::SegmentDisplay::new();
        select(arg, &mut segm_display);
        segm_display.show();
    }
}
